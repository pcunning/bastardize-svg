var through = require('through2');
var xml2js = require('xml2js');
var gutil = require('gulp-util');
var PluginError = gutil.PluginError;

const PLUGIN_NAME = "gulp-bastardize-svg";

function enumerateTag(tag, print) {
	for(var key in tag)
	{
		var prop = tag[key];
		if(typeof prop === "object")
		{
			if(prop.id !== undefined)
			{
				if(prop.class !== undefined && prop.class.indexOf(prop.id) < 0)
					prop.class = prop.class + " " + prop.id;
				else
					prop.class = prop.id;
			}
			if(print)
				console.dir(prop);
			tag[key] = enumerateTag(prop, print);
		}
	}
	return tag;
}

function gulpBastardizeSvg() {
	var stream = through.obj(function (file, enc, cb) {

		var builder = new xml2js.Builder();

		if(file.isStream())
		{
			this.emit('error', new PluginError(PLUGIN_NAME, 'Streams are not supported!'));
			return cb();
		}

		if(file.isBuffer())
		{
			// Get the file contents as a string
			var s = file.contents.toString();
			// Strip all newline/tab characters
			s = s.replace(/\r|\t|\n/g, "");
			// Parse the XML
			xml2js.parseString(s, function (err, result) {
				// If there was an error parsing
				if(err)
				{
					this.emit('error', new PluginError(PLUGIN_NAME, 'Error parsing file "' + file.path + '"'));
					return;
				}
				// Enumerate every tag
				var modifiedObj = enumerateTag(result.svg);
				// Build the output
				var xml = builder.buildObject(modifiedObj);
				xml = xml.replace(/root/g, "svg");
				// Update the buffer
				file.contents = new Buffer(xml);
			}.bind(this));
		}

		this.push(file);
		cb();
	});

	return stream;
}

module.exports = gulpBastardizeSvg;